package com.programmingtechie.inventoryservice.model;

public enum Role {
    USER,
    ADMIN
}
