package com.programmingtechie.inventoryservice.service.Impl;

import com.programmingtechie.inventoryservice.dto.JwtAuthenticationResponse;
import com.programmingtechie.inventoryservice.dto.SignInRequest;
import com.programmingtechie.inventoryservice.dto.SignUpRequest;
import com.programmingtechie.inventoryservice.model.Role;
import com.programmingtechie.inventoryservice.model.User;
import com.programmingtechie.inventoryservice.repository.UserRepository;
import com.programmingtechie.inventoryservice.service.AuthenticationService;
import com.programmingtechie.inventoryservice.service.JWTService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JWTService jwtService;

    public User signup(SignUpRequest signUpRequest){
        User user = new User();
        user.setEmail(signUpRequest.getEmail());
        user.setFirstname(signUpRequest.getFirstname());
        user.setLastname(signUpRequest.getLastname());
        user.setRole(Role.USER);
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        return userRepository.save(user);
    }
    public JwtAuthenticationResponse signin(SignInRequest signInRequest){
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(signInRequest.getEmail(),signInRequest.getPassword()));
        var user = userRepository.findByEmail(signInRequest.getEmail()).orElseThrow(() -> new IllegalArgumentException("Invalid Email or Password!!"));
        var jwt = jwtService.generateToken(user);

        var refershToken = jwtService.generateRefershToken(new HashMap<>(),user);

        JwtAuthenticationResponse jwtAuthenticationResponse = new JwtAuthenticationResponse();
        jwtAuthenticationResponse.setToken(jwt);
        jwtAuthenticationResponse.setRefreshToken(refershToken);
        return jwtAuthenticationResponse;

    }
}
