package com.programmingtechie.inventoryservice.service;

import com.programmingtechie.inventoryservice.dto.JwtAuthenticationResponse;
import com.programmingtechie.inventoryservice.dto.SignInRequest;
import com.programmingtechie.inventoryservice.dto.SignUpRequest;
import com.programmingtechie.inventoryservice.model.User;

public interface AuthenticationService {
    User signup(SignUpRequest signUpRequest);
    JwtAuthenticationResponse signin(SignInRequest signInRequest);
}
