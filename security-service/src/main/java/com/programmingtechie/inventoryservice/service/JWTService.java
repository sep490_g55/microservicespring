package com.programmingtechie.inventoryservice.service;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.Map;

public interface JWTService {
    String extractUserName(String token);
    String generateToken(UserDetails userDetails);
    boolean isTokenValid(String token,UserDetails userDetails);

    String generateRefershToken(Map<String,Object> extractClaims, UserDetails userDetails);
}
