package com.programmingtechie.inventoryservice.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class SignInRequest {
    @NotBlank(message = "Email or username is mandatory")
    @Pattern(regexp = "^(?:\\w+|\\w+([+\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.[a-zA-z]{2,4})+)$",
            message = "This field must be email or username")
    private String email;
    @NotBlank(message = "Password is mandatory")
    @Pattern(regexp = "^(?=.*[A-Za-z@])(?=.*[0-9])[A-Za-z0-9]{8,}$",
            message = "Password must contain at least 8 characters and include both letters and numbers")
    private String password;

}
