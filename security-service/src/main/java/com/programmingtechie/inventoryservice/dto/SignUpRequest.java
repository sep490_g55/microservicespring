package com.programmingtechie.inventoryservice.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class SignUpRequest {
    @NotBlank(message = "firstname name is mandatory")
    @Size(min = 4, message = "firstname name is greater than 3")
    private String firstname;
    @NotBlank(message = "lastname name is mandatory")
    @Size(min = 4, message = "lastname name is greater than 3")
    private String lastname;
    private String email;
    private String password;

}
