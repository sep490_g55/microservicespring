package com.programmingtechie.inventoryservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class JwtAuthenticationResponse {
    @JsonProperty("access_token")
    private String token;
    @JsonProperty("access_token")
    private String refreshToken;
}
