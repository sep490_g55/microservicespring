package com.programmingtechie.inventoryservice.controller;

import com.programmingtechie.inventoryservice.dto.JwtAuthenticationResponse;
import com.programmingtechie.inventoryservice.dto.SignInRequest;
import com.programmingtechie.inventoryservice.dto.SignUpRequest;
import com.programmingtechie.inventoryservice.model.User;
import com.programmingtechie.inventoryservice.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/auth")

public class AuthenticationController {
    private final AuthenticationService authenticationService;

    @PostMapping("/signup")
    public ResponseEntity<User> signup(@RequestBody SignUpRequest signUpRequest){
        return ResponseEntity.ok(authenticationService.signup(signUpRequest));
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtAuthenticationResponse> signin(@RequestBody SignInRequest signIpRequest){
        return ResponseEntity.ok(authenticationService.signin(signIpRequest));
    }

}
